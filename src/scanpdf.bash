# SPDX-FileCopyrightText: 2021 ch3 <ch3@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

DOCNAME=$1 # TODO check for emptiness, file overwrites
DOC_PARTS=""

# TODO:
# print usage/help
# make resolution input
# possibly remove tmp files
# make menu with possible options
# include ocr
#   - tesseract
# include post document scan processing
#   - unpaper
#   - scantailor-cli
# show status/progress (scanned pages, size, ...)
# show last scan
# show full document
# rescan (last/any) page
# scale down
# provide format (A4, A5, Perso, ...)
# batch mode

# Init ------------------------------------------------------------------------
TMP_DIR="/tmp/scan_tmp/"
mkdir -p $TMP_DIR
echo "Saving temporary files to \`$TMP_DIR\'"

COUNT=0
# /Init -----------------------------------------------------------------------

scan_next () {
	# Scan to temp format
	TMP_DOC_PART="$TMP_DIR"doc_$COUNT".tiff"
	echo "scanimage --resolution=300 --format=tiff > $TMP_DOC_PART"
	scanimage --resolution=300 --format=tiff > $TMP_DOC_PART

	# Convert to pdf
	DOC_PART="$TMP_DIR"doc_$COUNT".pdf"
	echo "convert $TMP_DOC_PART $DOC_PART"
	convert $TMP_DOC_PART $DOC_PART

	# Accumulate parts
	DOC_PARTS="$DOC_PARTS $DOC_PART"
	echo $DOC_PARTS

	let COUNT++
}

rescan () {
	echo rescan
}

finish () {
	# Merge all documents toghether
	echo "pdftk $DOC_PARTS cat output $DOCNAME"
	pdftk $DOC_PARTS cat output $DOCNAME
}

abort () {
	true
}

show_status () {
	true
}

show_menu () {
PS3='Action to take: '
select opt in "Scan Next Page (default)" "Finish" "Abort"; do
	case $REPLY in
		1|n|N|next|scan) scan_next;;
		2|f|finish) finish;;
		3|exit|abort) exit;;
		# r|re|rescan) rescan;;
		# configure
		# clean (tmp)
		# view full doc/current page
		# process current page
		# post-process full doc
		*) scan_next;;
	esac
done
}


while true
do
	show_menu;
done

# Merge all documents toghether
echo "pdftk $DOC_PARTS cat output $DOCNAME"
pdftk $DOC_PARTS cat output $DOCNAME
