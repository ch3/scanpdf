<!--
SPDX-FileCopyrightText: 2021 ch3 <ch3@mailbox.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# scanpdf

Simple script to help with scanning to pdf from the command line.